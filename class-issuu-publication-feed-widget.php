<?php
/**
 * Custom Widget for displaying newest publications
 *
 * Displays a link to the newest publication.
 *
 * @since 1.0.0
 */
class Issuu_Publication_Feed_Widget extends WP_Widget {

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 *
	 * @return Issuu_Publication_Feed_Widget
	 */
	 function __construct() {
		parent::__construct( 'widget_issuu_publication_feed', __( 'Recent Issuu Publications', 'issuu-publication-feed' ), array(
			'classname'   => 'widget_issuu_publication_feed',
			'description' => __( 'Use this widget to list your recent Issuu Publications.', 'issuu-publication-feed' ),
		) );
	}

	/**
	 * Output the HTML for this widget.
	 *
	 * @access public
	 * @since 1.0.0
	 *
	 * @param array $args     An array of standard parameters for widgets in this theme.
	 * @param array $instance An array of settings for this widget instance.
	 */
	public function widget( $args, $instance ) {
		$number = empty( $instance['number'] ) ? 2 : absint( $instance['number'] );
		$title  = $args['before_title'] . $instance['title'] . $args['after_title'];

		$issuu = new WP_Query( array(
			'order'          => 'DESC',
			'posts_per_page' => $number,
			'no_found_rows'  => true,
			'post_status'    => 'publish',
			'post_type'      => 'issuu_publication',
		) );
		if ( $issuu->have_posts() ) :

			echo $args['before_widget'];
			?>
			<?php echo $title; ?>
			<ol>

				<?php
					while ( $issuu->have_posts() ) :
						$issuu->the_post();
				?>
				<li>
				<article <?php post_class(); ?>>
					<div class="entry-content">
						<?php
							if ( post_password_required() ) :
								the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'issuu-publication-feed' ) );
							else :

								$post_thumbnail = get_the_post_thumbnail();
	
								if ( ! empty ( $post_thumbnail ) ) :
						?>
						<a href="<?php the_permalink(); ?>"><?php echo $post_thumbnail; ?></a>
						<?php endif; ?>
					<?php endif; ?>
					</div><!-- .entry-content -->

					<header class="entry-header">
						<div class="entry-meta">
							<?php the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' ); ?>
						</div><!-- .entry-meta -->
					</header><!-- .entry-header -->
				</article><!-- #post-## -->
				</li>
				<?php endwhile; ?>

			</ol>
			<a class="issuu-publication-archive-link" href="<?php echo get_post_type_archive_link( 'issuu_publication' ); ?>">
				<?php
					_e( 'More publications <span class="meta-nav">&rarr;</span>', 'issuu-publication-feed' );
				?>
			</a>
			<?php

			echo $args['after_widget'];

			// Reset the post globals as this query will have stomped on it.
			wp_reset_postdata();

		endif; // End check for issuu posts.
	}

	/**
	 * Deal with the settings when they are saved by the admin.
	 *
	 * Here is where any validation should happen.
	 *
	 * @since 1.0.0
	 *
	 * @param array $new_instance New widget instance.
	 * @param array $instance     Original widget instance.
	 * @return array Updated widget instance.
	 */
	function update( $new_instance, $instance ) {
		$instance['title']  = strip_tags( $new_instance['title'] );
		$instance['number'] = empty( $new_instance['number'] ) ? 2 : absint( $new_instance['number'] );

		return $instance;
	}


	/**
	 * Display the form for this widget on the Widgets page of the Admin area.
	 *
	 * @since 1.0.0
	 *
	 * @param array $instance
	 */
	function form( $instance ) {
		$title  = empty( $instance['title'] ) ? '' : esc_attr( $instance['title'] );
		$number = empty( $instance['number'] ) ? 2 : absint( $instance['number'] );
		$format = isset( $instance['format'] ) && in_array( $instance['format'], $this->formats ) ? $instance['format'] : 'aside';
		?>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'issuu-publication-feed' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"></p>

			<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of posts to show:', 'issuu-publication-feed' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3"></p>
		<?php
	}
}
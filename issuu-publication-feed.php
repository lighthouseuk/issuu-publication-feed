<?php
/*
Plugin Name: Issuu Publication Feed
Plugin URI:  https://bitbucket.org/lighthouseuk/issuu-publication-feed
Bitbucket Plugin URI: https://bitbucket.org/lighthouseuk/issuu-publication-feed
Release Asset: true
Description: Import your issuu feed as a post type
Version:     1.0.4
Author:      Mike Manger
Text Domain: issuu-publication-feed
Domain Path: /languages
License:     GNU General Public License v2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

require_once 'class-issuu-publication-feed-settings.php';
require_once 'class-issuu-publication-feed.php';

add_action( 'plugins_loaded', array( 'Issuu_Publication_Feed', 'get_instance' ) );
register_activation_hook( __FILE__, array( 'Issuu_Publication_Feed', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'Issuu_Publication_Feed', 'deactivate' ) );
register_uninstall_hook( __FILE__, array( 'Issuu_Publication_Feed', 'uninstall' ) );

if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) && ( ! defined( 'ISSUU_PUBLICATION_FEED_SHOW_ADMIN_SETTINGS' ) || ! ISSUU_PUBLICATION_FEED_SHOW_ADMIN_SETTINGS ) ) {
	require_once 'class-issuu-publication-feed-admin.php';
	add_action( 'plugins_loaded', array( 'Issuu_Publication_Feed_Admin', 'get_instance' ) );
}

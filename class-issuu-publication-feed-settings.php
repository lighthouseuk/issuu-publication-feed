<?php
/**
 * Issuu_Publication_Feed_Settings class. This class provides a wrapper for
 * getting the plugin settings.
 *
 * @since 1.0.0
 */
class Issuu_Publication_Feed_Settings {

	/**
	 * Issuu API URL.
	 *
	 * @since 1.0.0
	 *
	 * @var   string
	 */
	private static $api_url = 'http://api.issuu.com/1_0';

	/**
	 * Retrieve plugin option value based on name of option.
	 *
	 * If the option does not exist or does not have a value, then the return
	 * value will be false.
	 *
	 * @since 1.0.0
	 *
	 * @see get_option()
	 *
	 * @param string $option
	 * @param string $default
	 * @return mixed Value set for the option. Or false if it isn't set
	 */
	public static function get_option( $option, $default = false ) {
		$options = get_option( 'issuu_publication_feed' );

		if ( isset( $options[ $option ] ) ) {
			return $options[ $option ];
		} else {
			return $default;
		}
	}

	/**
	 * Get the issuu API URL.
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public static function get_api_url() {
		return self::$api_url;
	}

	/**
	 * Get the issuu API Key.
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public static function get_api_key() {
		if ( defined( 'ISSUU_PUBLICATION_FEED_API_KEY' ) ) {
			return ISSUU_PUBLICATION_FEED_API_KEY;
		} else {
			return self::get_option( 'api_key' );
		}
	}

	/**
	 * Get the issuu API Secret Key.
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public static function get_api_secret_key() {
		if ( defined( 'ISSUU_PUBLICATION_FEED_API_SECRET_KEY' ) ) {
			return ISSUU_PUBLICATION_FEED_API_SECRET_KEY;
		} else {
			return self::get_option( 'api_secret_key' );
		}
	}

	/**
	 * Returns if the current API key was valid when the settings were last saved.
	 *
	 * @since 1.0.0
	 *
	 * @return bool False if API key is not valid and true if it was when it was set.
	 */
	public static function is_api_key_valid() {
		return self::get_option( 'api_key_valid' );
	}

	/**
	 * Returns the publication embed width.
	 *
	 * @since 1.0.0
	 *
	 * @return int
	 */
	public static function get_embed_width() {
		if ( defined( 'ISSUU_PUBLICATION_FEED_EMBED_WIDTH' ) ) {
			$width = ISSUU_PUBLICATION_FEED_EMBED_WIDTH;
		} else {
			global $content_width;
			return self::get_option( 'embed_width', $content_width );
		}
	}

	/**
	 * Returns the publication embed height.
	 *
	 * @since 1.0.0
	 *
	 * @return int
	 */
	public static function get_embed_height() {
		if ( defined( 'ISSUU_PUBLICATION_FEED_EMBED_HEIGHT' ) ) {
			return ISSUU_PUBLICATION_FEED_EMBED_HEIGHT;
		} else {
			return self::get_option( 'embed_height' );
		}
	}

	/**
	 * Returns the publication thumbnail size.
	 *
	 * @since 1.0.0
	 *
	 * @return string Size of the image. Either none, large, medium or small. Defaults to large.
	 */
	public static function get_image_size() {
		if ( defined( 'ISSUU_PUBLICATION_FEED_IMAGE_SIZE' ) ) {
			return ISSUU_PUBLICATION_FEED_IMAGE_SIZE;
		} else {
			return self::get_option( 'image_size', 'large' );
		}
	}

	/**
	 * Returns whether tags are enabled or not.
	 *
	 * @since 1.0.0
	 *
	 * @return bool True if tags are enabled, false if they are not. Defaults to true.
	 */
	public static function tags_enabled() {
		if ( defined( 'ISSUU_PUBLICATION_TAGS_ENABLED' ) ) {
			return ISSUU_PUBLICATION_TAGS_ENABLED;
		} else {
			return self::get_option( 'tags_enabled', true );
		}
	}

	/**
	 * Returns whether folders (stacks) are enabled or not.
	 *
	 * @since 1.0.0
	 *
	 * @return bool True if folders are enabled, false if they are not. Defaults to true.
	 */
	public static function folders_enabled() {
		if ( defined( 'ISSUU_PUBLICATION_FOLDERS_ENABLED' ) ) {
			return ISSUU_PUBLICATION_FOLDERS_ENABLED;
		} else {
			return self::get_option( 'folders_enabled', true );
		}
	}

	/**
	 * Returns the publication post type slug.
	 *
	 * @since 1.0.0
	 *
	 * @return string Post URL slug. Defaults to issuu_publication
	 */
	public static function get_rewrite_slug() {
		if ( defined( 'ISSUU_PUBLICATION_FEED_REWRITE_SLUG' ) ) {
			return ISSUU_PUBLICATION_FEED_REWRITE_SLUG;
		} else {
			return self::get_option( 'rewrite_slug', 'issuu_publication' );
		}
	}

	/**
	 * Returns the folders (stacks) taxonomy slug.
	 *
	 * @since 1.0.0
	 *
	 * @return string Post URL slug. Defaults to issuu_publication_folders
	 */
	public static function get_folders_rewrite_slug() {
		if ( defined( 'ISSUU_PUBLICATION_FEED_FOLDERS_REWRITE_SLUG' ) ) {
			return ISSUU_PUBLICATION_FEED_FOLDERS_REWRITE_SLUG;
		} else {
			return self::get_option( 'folders_rewrite_slug', 'issuu_publication_folders' );
		}
	}
}
<?php
/**
 * Issuu_Publication_Feed class. This class setups up the plugin.
 *
 * @since 1.0.0
 */
class Issuu_Publication_Feed {

	/**
	 * Instance of this class.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	protected static $instance = null;

	function __construct() {
		// Check if rewrite rules have updated
		add_action( 'update_option_issuu_publication_feed', array( $this, 'validate_options' ), 10, 2 );

		add_action( 'init', array( $this, 'setup' ) );

		// Add the shortcode
		add_shortcode( 'issuu_publication_feed_document', array( $this, 'shortcode' ) );

		// Register cron hook
		add_action( 'issuu_publication_feed_import_event_hook', array( $this, 'import_event' ) );

		// Register AJAX import handler
		add_action( 'wp_ajax_issuu_publication_feed_import', array( $this, 'import_callback' ) );
		add_action( 'wp_ajax_nopriv_issuu_publication_feed_import', array( $this, 'import_callback' ) );

		// Register Widget
		add_action( 'widgets_init', array( $this, 'widgets_init' ) );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0.0
	 *
	 * @return object A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public static function activate() {
		if ( ! current_user_can( 'activate_plugins' ) ) {
			return;
		}
		$plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';
		check_admin_referer( "activate-plugin_{$plugin}" );

		wp_schedule_event( current_time( 'timestamp' ), 'daily', 'issuu_publication_feed_import_event_hook' );
	}

	public static function deactivate() {
		if ( ! current_user_can( 'activate_plugins' ) ) {
			return;
		}
		$plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';
		check_admin_referer( "deactivate-plugin_{$plugin}" );

		wp_clear_scheduled_hook( 'issuu_publication_feed_import_event_hook' );
	}

	public static function uninstall() {
		if ( ! current_user_can( 'activate_plugins' ) ) {
			return;
		}
		if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) && __FILE__ != WP_UNINSTALL_PLUGIN ) {
			return;
		}
		check_admin_referer( 'bulk-plugins' );

		delete_option( 'issuu_publication_feed' );

		// @TODO: Remove custom posts and postmeta
	}

	public function setup() {

		// Load plugin text domain
		load_plugin_textdomain( 'issuu-publication-feed', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );

		// Create or update database tables. Will only run when version changes
		$has_version_changed = get_option( 'issuu_publication_feed_db_version' ) != '1';
		if ( $has_version_changed ) {
			self::setup_database();
			update_option( 'issuu_publication_feed_db_version', '1' );
		}

		$taxonomies = array();
		if ( Issuu_Publication_Feed_Settings::folders_enabled() ) {
			$args = array(
				'label'             => __( 'Issuu Stacks', 'issuu-publication-feed' ),
				'show_admin_column' => true,
				'show_tagcloud'     => true,
				'rewrite' => array(
					'slug' => Issuu_Publication_Feed_Settings::get_folders_rewrite_slug(),
				),
			);
			$taxonomies[] = 'issuu_publication_folders';
			register_taxonomy( 'issuu_publication_folders', 'issuu_publication', $args );
		}
		if ( Issuu_Publication_Feed_Settings::tags_enabled() ) {
			//$taxonomies[] = 'issuu_publication_tags';
		}

		// Register plugin post type
		$args = array(
			'show_ui' => false,
			'public' => true,
			'has_archive' => true,
			'exclude_from_search' => false,
			'label' => __( 'Issuu Publications' , 'issuu-publication-feed' ),
			'rewrite' => array(
				'slug' => Issuu_Publication_Feed_Settings::get_rewrite_slug(),
			),
			'supports' => array( 'title', 'editor', 'thumbnail' ),
			'taxonomies' => $taxonomies,
		);
		register_post_type( 'issuu_publication', $args );
	}

	public function setup_database() {
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();
		$terms_table_name = $wpdb->prefix . 'issuu_publication_feed_folder_term';

		require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );

		$sql = "
CREATE TABLE `$terms_table_name` (
	`folder_key` CHAR(36) NOT NULL,
	`term_id` BIGINT(20) UNSIGNED NOT NULL,
	PRIMARY KEY (`folder_key`),
	INDEX `term_id` (`term_id`)
)
COMMENT='Stores the relation between issuu folder keys and term IDs'
$charset_collate;
";

		dbDelta( $sql );
	}

	/**
	 * Register widget.
	 *
	 * @since 1.0.0
	 */
	public function widgets_init() {
		require_once 'class-issuu-publication-feed-widget.php';
		register_widget( 'Issuu_Publication_Feed_Widget' );
	}

	/**
	 * Checks options after update to see if we need to flush the rewrite rules.
	 *
	 * @since 1.0.0
	 *
	 * @param array $old_settings
	 * @param array $new_settings
	 */
	public static function validate_options( $old_settings, $new_settings ) {
		// check if rewrite rules have updated
		if ( $old_settings['rewrite_slug'] != $new_settings['rewrite_slug'] ||
			$old_settings['folders_rewrite_slug'] != $new_settings['folders_rewrite_slug']
		) {
			// Rebuild the post type with the new slug
			self::setup();
			flush_rewrite_rules();
		}
	}

	/**
	 * Ajax callback for publication import.
	 *
	 * @since 1.0.0
	 *
	 * @see Issuu_Publication_Feed_API_Wrapper::import()
	 */
	public static function import_callback() {
		include_once 'class-issuu-publication-feed-api-wrapper.php';

		// Update our Issuu folders first
		if ( $_POST['start_index'] == 0 ) {
			Issuu_Publication_Feed_API_Wrapper::import_folders();
		}
		$response = Issuu_Publication_Feed_API_Wrapper::import( $_POST['start_index'], 10 );
		if ( is_wp_error( $response ) ) {
			wp_send_json_error();
		}
		wp_send_json_success( $response );
	}

	/**
	 * Print JavaScript to run AJAX import action.
	 *
	 * @since 1.0.0
	 *
	 * @see $this::import_callback()
	 */
	public static function print_import_script( $automatic = false ) {
		?>
		<script type="text/javascript">
			jQuery(document).ready( function() {
				function hideSpinner() {
					jQuery( 'a.issuu-publication-feed-import .spinner, .issuu-progress' ).hide();
				}
				function importData( startIndex ) {
					var data = {
						'action': 'issuu_publication_feed_import',
						'start_index': startIndex
					};

					jQuery.post( '<?php echo admin_url( 'admin-ajax.php' ); ?>', data, function( response ) {
						if ( response.success ) {
							jQuery( '.issuu-progress' ).val( startIndex + response.data.processed )
								.attr( 'max', response.data.total_count );
							if ( response.data.have_more ) {
								importData( startIndex + response.data.processed );
							} else {
								hideSpinner();
							}
						} else {
							<?php if ( ! $automatic ) : ?>
							alert('API error, check API key is correct');
							<?php endif; ?>
							hideSpinner();
						}
					}, 'json' );
				}
				<?php if ( $automatic ) : ?>
					importData( 0 );
				<?php else : ?>
					jQuery( 'a.issuu-publication-feed-import' ).click( function(e) {
						e.preventDefault();
						jQuery( this ).find( '.spinner' ).show();
						jQuery( '.issuu-progress' ).show();
						importData( 0 );
					} );
				<?php endif; ?>
			} );
		</script>
		<?php
	}

	/**
	 * WordPress cron event to add import script to site footer.
	 *
	 * @since 1.0.0
	 */
	public function import_event() {
		add_action( 'wp_footer', array( $this, 'print_import_script' ) );
	}

	/**
	 * Remove all issuu custom posts and their metadata from site.
	 *
	 * @since 1.0.0
	 */
	public static function delete_all_posts( $force_delete = true ) {
		$posts = new WP_Query( array(
			'posts_per_page' => -1,
			'post_type'      => 'issuu_publication',
			'fields'         => 'ids',
		) );

		foreach ( $posts->posts as $post_id ) {
			wp_delete_post( $post_id, $force_delete );
		}
	}

	/**
	 * Output document embed code with shortcode.
	 *
	 * @since 1.0.0
	 */
	public function shortcode( $atts ) {
		$default_atts = array(
			'post_id' => get_the_ID(),
		);

		$atts = shortcode_atts( $default_atts, $atts );

		$meta_data = get_post_meta( $atts['post_id'], 'issuu_publication_feed_meta', true );
		if ( ! is_array( $meta_data ) ) {
			return '';
		}
		$width = $meta_data['embed_width'] ?? 0;
		$height = $meta_data['embed_height'] ?? 0;

		$css = '';
		if ( $width > 0 ) {
			$css = 'width: ' . $width . 'px; ';
		}
		if ( $height > 0 ) {
			$css .= 'height: ' . $height . 'px;';
		}

		$output = '<div data-configid="' . $meta_data['embed_config_id'] . '" style="' . $css . '" class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>';
		return $output;
	}

	/**
	 * Check if publication exists and return the post ID or false if the publication doesn't exist.
	 *
	 * @param string $post_name Unique post name of the publication
	 *
	 * @return int|bool
	 */
	public static function publication_exists( $post_name ) {
		global $wpdb;

		$check_sql = "SELECT id FROM $wpdb->posts WHERE post_name = %s AND post_type = 'issuu_publication' LIMIT 1";
		$post_name_check = $wpdb->get_var( $wpdb->prepare( $check_sql, $post_name ) );

		if ( $post_name_check ) {
			return $post_name_check;
		} else {
			return false;
		}
	}

}

msgid ""
msgstr ""
"Project-Id-Version: Issuu Publication Feed\n"
"POT-Creation-Date: 2014-12-09 15:11-0000\n"
"PO-Revision-Date: 2014-12-09 15:11-0000\n"
"Last-Translator: Mike Manger <mike.manger@lighthouseuk.net>\n"
"Language-Team: Lighthouse <technical@lighthouseuk.net>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.7.1\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_x;_n;_nx;_n_noop;_nx_noop;"
"translate_nooped_plural;esc_html__;esc_html_e;esc_html_x;esc_attr__;"
"esc_attr_e;esc_attr_x\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../class-issuu-publication-feed-admin.php:62
msgid "Issuu Publication Feed Settings"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:62
msgid "Issuu"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:71
msgid "Issuu Settings"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:87
msgid "API Settings"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:94
msgid "API Key"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:101
#: ../class-issuu-publication-feed-admin.php:222
msgid "placeholdertext"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:110
msgid "API Secret Key"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:113
msgid "Front end settings"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:121
msgid "Width"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:129
msgid "Height"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:136
msgid "No image"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:137
msgid "Large"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:138
msgid "Medium"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:139
msgid "Small"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:142
msgid "Image Size"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:149
msgid "Rewrite Slug"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:161
#, php-format
msgid ""
"<a href=\"%s\" target=\"_blank\">Get an API Key</a>, then enter your details "
"here"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:173
msgid "API Key valid"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:174
#, php-format
msgid "Next import scheduled is for %s"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:178
#, php-format
msgid ""
"<a href=\"%s\" class=\"button issuu-publication-feed-import\">Run import "
"now<span class=\"spinner\"></span></a>"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:194
msgid "API Key is not valid"
msgstr ""

#: ../class-issuu-publication-feed-admin.php:204
msgid "Desired embed size in pixels"
msgstr ""

#: ../class-issuu-publication-feed-api-wrapper.php:143
#, php-format
msgid "%s page 1"
msgstr ""

#: ../class-issuu-publication-feed-widget.php:19
msgid "Recent Issuu Publications"
msgstr ""

#: ../class-issuu-publication-feed-widget.php:21
msgid "Use this widget to list your recent Issuu Publications."
msgstr ""

#: ../class-issuu-publication-feed-widget.php:61
msgid "Continue reading <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: ../class-issuu-publication-feed-widget.php:85
msgid "More publications <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: ../class-issuu-publication-feed-widget.php:129
msgid "Title:"
msgstr ""

#: ../class-issuu-publication-feed-widget.php:132
msgid "Number of posts to show:"
msgstr ""

#: ../class-issuu-publication-feed.php:97
msgid "Issuu Publications"
msgstr ""

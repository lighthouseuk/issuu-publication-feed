<?php
/**
 * Issuu_Publication_Feed_Admin class. This class adds settings pages to
 * the administrative side of the WordPress site.
 *
 * @since 1.0.0
 */
class Issuu_Publication_Feed_Admin {

	/**
	 * Instance of this class.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	protected static $instance = null;

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'register_menu' ) );
		add_action( 'admin_init', array( $this, 'register_settings' ) );
		$plugin_dir = dirname( plugin_basename( __FILE__ ) );
		add_filter( 'plugin_action_links_' . $plugin_dir . '/issuu-publication-feed.php', array( $this, 'add_plugin_action_links' ) );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since  1.0.0
	 *
	 * @return object A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Adds settings link on plugins page.
	 *
	 * @since 1.0.0
	 *
	 * @param array $links Array of current links.
	 * @return array Filtered array of links
	 */
	function add_plugin_action_links( $links ) {
		$links['settings'] = '<a href="options-general.php?page=issuu-publication-feed">Settings</a>';
		return $links;
	}

	/**
	 * Registers settings page
	 *
	 * @since 1.0.0
	 */
	function register_menu() {
		add_submenu_page( 'options-general.php', __( 'Issuu Publication Feed Settings', 'issuu-publication-feed' ), __( 'Issuu' , 'issuu-publication-feed' ), 'manage_options', 'issuu-publication-feed', array( $this, 'settings_page_callback' ) );
	}

	/**
	 * Settings page callback used to display the settings.
	 *
	 * @since 1.0.0
	 */
	function settings_page_callback() {
		echo '<h2>' . __( 'Issuu Settings', 'issuu-publication-feed' ) . '</h2>';
		echo '<form action="options.php" method="post">';
		settings_fields( 'issuu-publication-feed-options' );
		do_settings_sections( 'issuu-publication-feed' );
		submit_button();
		echo '</form>';
	}

	/**
	 * Registers settings
	 *
	 * @since 1.0.0
	 */
	function register_settings() {
		register_setting( 'issuu-publication-feed-options', 'issuu_publication_feed', array( $this, 'validate_settings_callback' ) );
		// Add API settings section
		add_settings_section( 'issuu-publication-feed-api-section', __( 'API Settings', 'issuu-publication-feed' ), array( $this, 'api_settings_section_callback' ), 'issuu-publication-feed' );
		$args = array(
			'label_for' => 'issuu-publication-feed-api-key',
			'name'      => 'api_key',
			'required'  => true,
			'value'     => Issuu_Publication_Feed_Settings::get_api_key(),
		);
		add_settings_field( 'issuu-publication-feed-api-key', __( 'API Key', 'issuu-publication-feed' ), array( $this, 'input_string' ), 'issuu-publication-feed', 'issuu-publication-feed-api-section', $args );

		//don't print the secret key on the page
		$secret_key = Issuu_Publication_Feed_Settings::get_api_secret_key();
		if ( empty( $secret_key ) ) {
			$value = '';
		} else {
			$value = _x( 'placeholdertext', 'Used to mask password field', 'issuu-publication-feed' );
		}
		$args = array(
			'label_for' => 'issuu-publication-feed-api-secret-key',
			'name'      => 'api_secret_key',
			'type'      => 'password',
			'required'  => true,
			'value'     => $value,
		);
		add_settings_field( 'issuu-publication-feed-api-secret-key', __( 'API Secret Key', 'issuu-publication-feed' ), array( $this, 'input_string' ), 'issuu-publication-feed', 'issuu-publication-feed-api-section', $args );

		// Add front-end settings section
		// TODO: Check if API is able to access embed code
		add_settings_section( 'issuu-publication-feed-front-end-section', __( 'Front end settings', 'issuu-publication-feed' ), array( $this, 'front_end_settings_section_callback' ), 'issuu-publication-feed' );

		$args = array(
			'label_for' => 'issuu-publication-feed-embed-width',
			'name'      => 'embed_width',
			'type'      => 'number',
			'value'     => Issuu_Publication_Feed_Settings::get_embed_width(),
		);
		add_settings_field( 'issuu-publication-feed-embed-width', __( 'Width', 'issuu-publication-feed' ), array( $this, 'input_string' ), 'issuu-publication-feed', 'issuu-publication-feed-front-end-section', $args );

		$args = array(
			'label_for' => 'issuu-publication-feed-embed-height',
			'name'      => 'embed_height',
			'type'      => 'number',
			'value'     => Issuu_Publication_Feed_Settings::get_embed_height(),
		);
		add_settings_field( 'issuu-publication-feed-embed-height', __( 'Height', 'issuu-publication-feed' ), array( $this, 'input_string' ), 'issuu-publication-feed', 'issuu-publication-feed-front-end-section', $args );

		$args = array(
			'label_for' => 'issuu-publication-feed-image-size',
			'name'      => 'image_size',
			'value'     => Issuu_Publication_Feed_Settings::get_image_size(),
			'options'   => array(
				'none'   => _x( 'No image', 'Post thumbnail size', 'issuu-publication-feed' ),
				'large'  => _x( 'Large', 'Post thumbnail size', 'issuu-publication-feed' ),
				'medium' => _x( 'Medium', 'Post thumbnail size', 'issuu-publication-feed' ),
				'small'  => _x( 'Small', 'Post thumbnail size', 'issuu-publication-feed' ),
			),
		);
		add_settings_field( 'issuu-publication-feed-image-size', __( 'Image Size', 'issuu-publication-feed' ), array( $this, 'input_select' ), 'issuu-publication-feed', 'issuu-publication-feed-front-end-section', $args );

		$args = array(
			'label_for' => 'issuu-publication-feed-rewrite-slug',
			'name'      => 'rewrite_slug',
			'value'     => Issuu_Publication_Feed_Settings::get_rewrite_slug(),
		);
		add_settings_field( 'issuu-publication-feed-rewrite-slug', __( 'Rewrite Slug', 'issuu-publication-feed' ), array( $this, 'input_string' ), 'issuu-publication-feed', 'issuu-publication-feed-front-end-section', $args );

		$args = array(
			'label_for' => 'issuu-publication-feed-folders-rewrite-slug',
			'name'      => 'folders_rewrite_slug',
			'value'     => Issuu_Publication_Feed_Settings::get_folders_rewrite_slug(),
		);
		add_settings_field( 'issuu-publication-feed-folders-rewrite-slug', __( 'Stacks Rewrite Slug', 'issuu-publication-feed' ), array( $this, 'input_string' ), 'issuu-publication-feed', 'issuu-publication-feed-front-end-section', $args );


	}

	/**
	 * API settings section callback to display API instructions and status.
	 *
	 * @since 1.0.0
	 * @TODO: move $_GET checks to init to fix wp_redirect calls firing after header
	 */
	function api_settings_section_callback() {
		$api_key_url = 'http://issuu.com/home/settings/apikey';
		echo '<p>' . sprintf( __( '<a href="%s" target="_blank">Get an API Key</a>, then enter your details here', 'issuu-publication-feed' ), $api_key_url ) . '</p>';
		if ( ! Issuu_Publication_Feed_Settings::get_api_key() ) {
			return;
		}
		if ( Issuu_Publication_Feed_Settings::is_api_key_valid() ) {
			if ( isset( $_GET['run-import'] ) && 'now' == $_GET['run-import'] ) {
				include_once 'class-issuu-publication-feed-api-wrapper.php';
				Issuu_Publication_Feed_API_Wrapper::import();
				// Redirect to prevent re-running on refresh
				wp_redirect( add_query_arg( 'run-import', 'done' ) );
				exit;
			}

			if ( isset( $_GET['delete-issuu-posts'] ) && 'now' == $_GET['delete-issuu-posts'] ) {
				Issuu_Publication_Feed::delete_all_posts();
				// Redirect to prevent re-running on refresh
				wp_redirect( add_query_arg( 'delete-issuu-posts', 'done' ) );
				exit;
			}

			$next_schedule = wp_next_scheduled( 'issuu_publication_feed_import_event_hook' );

			echo '<p style="color: green;">' . __( 'API Key valid', 'issuu-publication-feed' );
			printf( ' - ' . __( 'Next import scheduled is for %s', 'issuu-publication-feed' ), date_i18n( get_option( 'time_format' ), $next_schedule ) );
			echo '</p><p>';
			$post_count = wp_count_posts( 'issuu_publication' );
			$stack_count = wp_count_terms( 'issuu_publication_folders' );

			if ( isset( $post_count ) ) {
				$post_count = $post_count->publish;
			} else {
				$post_count = 0;
			}

			printf( _n( '%d post in system.', '%d posts in system.', $post_count, 'issuu-publication-feed' ), $post_count );
			printf( '<br />' . _n( '%d stack in system.', '%d stacks in system.', $stack_count, 'issuu-publication-feed' ), $stack_count );
			echo '</p><p>';
			$run_now_url = esc_url( add_query_arg( 'run-import', 'now' ) );
			printf( __( '<a href="%s" class="button issuu-publication-feed-import">Run import now<span class="spinner"></span></a>', 'issuu-publication-feed' ), $run_now_url );
			if ( $post_count ) {
				$delete_posts_url = esc_url( add_query_arg( 'delete-issuu-posts', 'now' ) );
				printf( '&nbsp;' . __( '<a href="%s" class="issuu-publication-feed-delete">Delete all posts</a>', 'issuu-publication-feed' ), $delete_posts_url );
			}
			echo '</p>';
			echo '<p><progress class="issuu-progress hidden" value="0" max="100"></progress></p>';

			/*
			 * JavaScript to display the spinner when import button is clicked.
			 */
			Issuu_Publication_Feed::print_import_script( false );
		} else {
			echo '<p style="color: red;">' . __( 'API Key is not valid', 'issuu-publication-feed' ) . '</p>';
		}
	}

	/**
	 * Embed settings section callback.
	 *
	 * @since 1.0.0
	 */
	function front_end_settings_section_callback() {
		echo '<p>' . __( 'Desired embed size in pixels', 'issuu-publication-feed' ) . '</p>';
	}

	/**
	 * Validate settings on save. Checks to see if API key is valid.
	 *
	 * @since 1.0.0
	 *
	 * @param array $input Array of inputs to validate.
	 * @return array Filtered array of input values.
	 */
	function validate_settings_callback( $input ) {
		include_once 'class-issuu-publication-feed-api-wrapper.php';

		$input['api_key'] = trim( $input['api_key'] );
		$input['api_secret_key'] = trim( $input['api_secret_key'] );

		// Replace placeholder secret key with actual key
		if ( _x( 'placeholdertext', 'Used to mask password field', 'issuu-publication-feed' ) == $input['api_secret_key'] ) {
			$input['api_secret_key'] = Issuu_Publication_Feed_Settings::get_api_secret_key();
		}
		$input['embed_width'] = intval( $input['embed_width'] );
		$input['embed_height'] = intval( $input['embed_height'] );

		if ( ! in_array( $input['image_size'], array( 'none', 'large', 'medium', 'small' ) ) ) {
			$input['image_size'] = 'large';
		}

		// check if API is valid
		$input['api_key_valid'] = Issuu_Publication_Feed_API_Wrapper::is_api_valid( $input['api_key'], $input['api_secret_key'] );

		return $input;
	}

	/**
	 * Outputs an input HTML element
	 *
	 * @since 1.0.0
	 *
	 * @see add_settings_field()
	 *
	 * @param array $args {
	 *     An array of arguments
	 *
	 *     @string string $label_for Unique identifier for the input's label.
	 *     @string string $name      The input's option name.
	 *     @string string $type      The input type. Defaults to 'text'.
	 *     @string string $required  If the input element should have the required attribute. Defaults to false.
	 *     @string string $value     Input's value.
	 * }
	 */
	function input_string( $args ) {
		$defaults = array(
			'label_for' => '',
			'name'      => '',
			'type'      => 'text',
			'required'  => false,
			'value'     => '',
		);
		$args = wp_parse_args( $args, $defaults );

		if ( empty( $args['label_for'] ) ) {
			$id = '';
		} else {
			$id = ' id="' . $args['label_for'] . '"';
		}

		if ( $args['required'] ) {
			$required = ' required="required"';
		} else {
			$required = '';
		}
		echo '<input type="' . $args['type'] . '" name="issuu_publication_feed[' . $args['name'] . ']" value="' . $args['value'] . '"' . $id . $required . ' />';
	}

	/**
	 * Outputs a select HTML element
	 *
	 * @since 1.0.0
	 *
	 * @see add_settings_field()
	 *
	 * @param array $args {
	 *     An array of arguments
	 *
	 *     @string string $label_for Unique identifier for the select's label.
	 *     @string string $name      The select option name.
	 *     @string string $value     Selected option value.
	 *     @string array $options    Array of select options defined by key => value pairs.
	 * }
	 */
	function input_select( $args ) {
		$defaults = array(
			'label_for' => '',
			'name'      => '',
			'value'     => '',
			'options'   => array(),
		);
		$args = wp_parse_args( $args, $defaults );

		if ( empty( $args['label_for'] ) ) {
			$id = '';
		} else {
			$id = ' id="' . $args['label_for'] . '"';
		}

		echo '<select name="issuu_publication_feed[' . $args['name'] . ']" ' . $id . '>';
		foreach ( $args['options'] as $key => $value ) {
			echo '<option value="' . $key . '" ' . selected( $key, $args['value'] ) . '">' . $value . '</option>';
		}
		echo '</select>';
	}
}
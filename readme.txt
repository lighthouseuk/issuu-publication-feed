=== Issuu Publication Feed ===
Contributors: mikemanger
Tags:
Requires at least: 4.1.0
Tested up to: 6.3.1
Stable tag: 1.0.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A simple plugin to embed Issuu publications.

== Description ==

An unofficial plugin to embed [Issuu](https://issuu.com/) publications.

== Installation ==

1. Upload `issuu-publication-feed` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= I have found a bug =

Please raise a ticket on the [issue tracker](https://bitbucket.org/lighthouseuk/issuu-publication-feed/issues). Pull requests also accepted!

== Changelog ==

= 1.0.5 =
* Handle invalid API keys
* Speed up import process

= 1.0.4 =
* Check meta data is valid in shortcode

= 1.0.3 =
* Fix PHP7 error in cron task

= 1.0.2 =
* Add [Github Updater](https://github.com/afragen/github-updater/) asset support

= 1.0.1 =
* Small PHP parsing fix

= 1.0 =
* First public release

<?php
/**
 * Issuu_Publication_Feed_API_Wrapper class. This class provides a wrapper for
 * the Issuu API.
 *
 * @since 1.0.0
 */
class Issuu_Publication_Feed_API_Wrapper {

	/**
	 * Check if Issuu credentials are correct by querying document list
	 *
	 * @since 1.0.0
	 *
	 * @param string $api_key API key to check. Defaults to current key. Optional
	 * @param string $api_secret_key Secret key to check. Defaults to current key. Optional
	 * @param string $api_url Issuu API URL. Defaults to current URL. Optional
	 * @return bool True if the API is valid, false if there was an error.
	 */
	public static function is_api_valid( $api_key = null, $api_secret_key = null, $api_url = null ) {
		include_once 'lib/issuu-api.php';
		if ( is_null( $api_key ) ) {
			$api_key = Issuu_Publication_Feed_Settings::get_api_key();
		}

		if ( is_null( $api_secret_key ) ) {
			$api_secret_key = Issuu_Publication_Feed_Settings::get_api_secret_key();
		}

		if ( is_null( $api_url ) ) {
			$api_url = Issuu_Publication_Feed_Settings::get_api_url();
		}

		$issuu = new IssuuClient( $api_key, $api_secret_key, $api_url );
		$issuu->openAction( 'issuu.documents.list' );
		$issuu->__set( 'pageSize', '0' );
		$issuu->__set( 'responseParams', 'documentId' );
		$issuu->executeAction();
		$response = $issuu->getResponse();
		$issuu->closeAction();

		return 'ok' == $response['rsp']['stat'];
	}

	/**
	 * Imports active issuu publications into custom post type.
	 *
	 * @since 1.0.0
	 *
	 * @param int $start_index Publication index to start import from. Defaults to 0.
	 * @param int $results Number of results to import. If set to 0 it will import all publications. Defaults to 0.
	 * @return object Specifying how many results are imported.
	 */
	public static function import( $start_index = 0, $results = 0 ) {

		// should be moved somewhere?
		include_once 'lib/issuu-api.php';

		$api_key        = Issuu_Publication_Feed_Settings::get_api_key();
		$api_secret_key = Issuu_Publication_Feed_Settings::get_api_secret_key();
		$api_url        = Issuu_Publication_Feed_Settings::get_api_url();

		$embed_width  = Issuu_Publication_Feed_Settings::get_embed_width();
		$embed_height = Issuu_Publication_Feed_Settings::get_embed_height();

		$image_size = Issuu_Publication_Feed_Settings::get_image_size();

		$folders_enabled = Issuu_Publication_Feed_Settings::folders_enabled();
		$tags_enabled    = Issuu_Publication_Feed_Settings::tags_enabled();

		$issuu = new IssuuClient( $api_key, $api_secret_key, $api_url );
		$issuu->openAction( 'issuu.documents.list' );

		// We only want publicly listed publications
		$issuu->__set( 'access', 'public' );

		/*
		 * We are only interested in Active documents. Not setting this parameter would
		 * include failed uploads and documents still being processed.
		 */
		$issuu->__set( 'documentStates', 'A' );

		$response_params = 'documentId,username,name,title,publishDate';

		if ( $folders_enabled ) {
			$response_params .= ',folders';
		}
		if ( $tags_enabled ) {
			$response_params .= ',tags';
		}

		$issuu->__set( 'responseParams', $response_params );
		$index = $start_index;
		$have_more = true;
		$import_count = 0;

		/*
		 * Loop until we have requested number of publications
		 */
		do {
			$issuu->__set( 'startIndex', $index );
			$issuu->executeAction();
			$response = $issuu->getResponse();

			// exit if there is a problem with the API
			if ( 'ok' != $response['rsp']['stat'] ) {
				return new WP_Error('api', 'API issue');
			}

			// The API tells us if there is more to collect
			$have_more = $response['rsp']['_content']['result']['more'];

			// Increase the start index for the next loop
			$index += $response['rsp']['_content']['result']['pageSize'];

			$publications = $response['rsp']['_content']['result']['_content'];
			foreach ( $publications as $pub ) {
				// issuu allow dots in their post slugs so lets replace them
				$post_name = str_replace( '.', '-', $pub['document']['name'] );

				$pub_exists = Issuu_Publication_Feed::publication_exists( $post_name );

				// Skip rows if we have enough results.
				if ( $results > 0 && $import_count > $results ) {
					continue;
				}

				$import_count++;

				// Update publication title if it already exists
				if ( $pub_exists ) {
					$post_args = array(
						'ID'         => $pub_exists,
						'post_title' => $pub['document']['title'],
					);
					$post_id = wp_update_post( $post_args );
				} else {
					$post_args = array(
						'post_type'     => 'issuu_publication',
						'post_title'    => $pub['document']['title'],
						'post_name'     => $post_name,
						'post_content'  => '[issuu_publication_feed_document]',
						'post_status'   => 'publish',
						'post_date'     => $pub['document']['publishDate'],
						'post_date_gtm' => $pub['document']['publishDate'],
					);
					$post_id = wp_insert_post( $post_args );
				}

				// Set our folder information
				if ( $folders_enabled && isset( $pub['document']['folders'] ) ) {
					$folders = $pub['document']['folders'];
					self::set_publication_folders( $post_id, $folders );
				}

				// Now we have a post, add our metadata
				if ( $post_id ) {
					$embed_config_id = false;
					if ( $pub_exists ) {
						// This shouldn't change and speeds up the process.
						$existing_data = get_post_meta( $post_id, 'issuu_publication_feed_meta', true );
						if ( is_array( $existing_data ) && isset( $existing_data['embed_config_id'] ) ) {
							$embed_config_id = $existing_data['embed_config_id'];
						}
					}
					if ( ! $embed_config_id ) {
						$embed_config_id = self::get_embed_config_id( $pub['document']['documentId'], $embed_width, $embed_height );
					}
					$meta_array = array(
						'document_id'     => $pub['document']['documentId'],
						'document_name'   => $pub['document']['name'],
						'username'        => $pub['document']['username'],
						'embed_config_id' => $embed_config_id,
						'embed_width'     => $embed_width,
						'embed_height'    => $embed_height,
					);
					update_post_meta( $post_id, 'issuu_publication_feed_meta', $meta_array );

					// TODO: be smart about updating images
					if ( 'none' != $image_size && ! has_post_thumbnail( $post_id ) ) {
						$image_url = 'https://image.issuu.com/' . $pub['document']['documentId'] . '/jpg/page_1_thumb_' . $image_size . '.jpg';
						$image_desc = sprintf( _x( '%s page 1', 'Publication thumbnail description', 'issuu-publication-feed' ), $pub['document']['name'] );
						self::set_post_thumbnail( $post_id, $image_url, $post_name, $image_desc );
					}
				}

			}
		} while ( $have_more && ( $results == 0 || $import_count < $results ) );
		$issuu->closeAction();

		$result = new stdClass();
		$result->have_more   = $have_more;
		$result->state       = $response['rsp']['stat'];
		$result->processed   = $import_count;
		$result->total_count = $response['rsp']['_content']['result']['totalCount'];
		return $result;
	}

	/**
	 * Sets issuu stack folders for a publication.
	 * Takes an array of folder keys and decodes them into the relevant term IDs.
	 * Does not add the folders if they don't exist.
	 *
	 * @see wp_set_object_terms
	 *
	 * @param int $post_id Publication post ID to set the folders
	 * @param array $folders array of strings containing Issuu folder keys
	 */
	public static function set_publication_folders( $post_id, $folders ) {
		$terms = array();
		foreach ( $folders as $folder_key ) {
			$has_folder_key = self::folder_exists( $folder_key );
			if ( $has_folder_key ) {
				$terms[] = $has_folder_key;
			}
		}
		wp_set_object_terms( $post_id, $terms, 'issuu_publication_folders' );
	}

	/**
	 * Checks to see if an Issuu folder key has being indexed against a WordPress term.
	 *
	 * @param string $folder_key Issuu folder key to check.
	 * @return int Term ID, 0 if it doesn't exist
	 */
	public static function folder_exists( $folder_key ) {
		global $wpdb;
		$terms_table_name = $wpdb->prefix . 'issuu_publication_feed_folder_term';
		$sql = "SELECT term_id FROM $terms_table_name WHERE folder_key = %s";
		$result = $wpdb->get_var( $wpdb->prepare( $sql, $folder_key ) );

		return intval( $result );
	}

	/**
	 * Map a unique Issuu folder key against a term ID
	 *
	 * @param string $folder_key Issuu folder key to map.
	 * @param int $term_id Term to map key against.
	 */
	public static function map_folder_key( $folder_key, $term_id ) {
		global $wpdb;
		$terms_table_name = $wpdb->prefix . 'issuu_publication_feed_folder_term';
		$data = array(
			'folder_key' => $folder_key,
			'term_id'    => $term_id,
		);
		$format = array(
			'%s',
			'%d',
		);
		$wpdb->insert( $terms_table_name, $data, $format );
	}

	/**
	 * Import all Issuu folders as WordPress terms
	 */
	public static function import_folders() {
		include_once 'lib/issuu-api.php';

		$api_key        = Issuu_Publication_Feed_Settings::get_api_key();
		$api_secret_key = Issuu_Publication_Feed_Settings::get_api_secret_key();
		$api_url        = Issuu_Publication_Feed_Settings::get_api_url();

		$issuu = new IssuuClient( $api_key, $api_secret_key, $api_url );
		$issuu->__set( 'responseParams', 'folderId,name,description' );
		$issuu->openAction( 'issuu.folders.list' );

		$index = 0;

		// Stores array of active term IDs
		$active_terms = array();

		/*
		 * Loop until we have requested number of publications
		 */
		do {
			$issuu->__set( 'startIndex', $index );
			$issuu->executeAction();
			$response = $issuu->getResponse();

			// exit if there is a problem with the API
			if ( 'ok' != $response['rsp']['stat'] ) {
				return;
			}

			// The API tells us if there is more to collect
			$have_more = $response['rsp']['_content']['result']['more'];

			// Increase the start index for the next loop
			$index += $response['rsp']['_content']['result']['pageSize'];

			$folders = $response['rsp']['_content']['result']['_content'];
			foreach ( $folders as $folder ) {
				$folder_exists = self::folder_exists( $folder['folder']['folderId'] );
				$args = array();
				if ( isset( $folder['folder']['description'] ) ) {
					$args['description'] = $folder['folder']['description'];
				}
				if ( $folder_exists ) {
					$args['name'] = $folder['folder']['name'];
					$term_array = wp_update_term( $folder_exists, 'issuu_publication_folders', $args );
				} else {
					$term_array = wp_insert_term( $folder['folder']['name'], 'issuu_publication_folders', $args );
					if ( ! is_wp_error( $term_array ) ) {
						self::map_folder_key( $folder['folder']['folderId'], $term_array['term_id'] );
					}
				}

				if ( ! is_wp_error( $term_array ) ) {
					$active_terms[] = $term_array['term_id'];
				}
			}
		} while ( $have_more );

		// Remove folders not found on Issuu.
		self::purge_folders( $active_terms );
	}

	/**
	 * Remove all Stack folders.
	 *
	 * @param array $folders_to_keep array of term IDs to keep.
	 */
	public static function purge_folders( $folders_to_keep = array() ) {
		global $wpdb;
		$terms_table_name = $wpdb->prefix . 'issuu_publication_feed_folder_term';

		$sql = "SELECT term_id FROM {$terms_table_name}";
		$where = '';
		if ( ! empty( $folders_to_keep ) ) {
			$where = ' WHERE term_id NOT IN ( ' . implode( ',', $folders_to_keep ) . ' )';
			$sql .= $where;
		}
		$terms = $wpdb->get_results( $sql );

		if ( empty( $terms ) ) {
			return;
		}

		foreach ( $terms as $term ) {
			wp_delete_term( $term->term_id, 'issuu_publication_folders' );
		}

		// Remove from our map table
		$delete_sql = "DELETE FROM {$terms_table_name}" . $where;
		$wpdb->query( $delete_sql );
	}

	/**
	 * Sets a featured image from an external source.
	 *
	 * @since 1.0.0
	 *
	 * @see set_post_thumbnail()
	 *
	 * @param int $post_id      Post to assign the image to.
	 * @param string $image_url External image url to download. Currently only supposts jpg.
	 * @param string $file_name Filename to save the image as. Optional.
	 * @param string $desc      Media library description. Optional.
	 *
	 * @return int|WP_Error Returns id of thumbnail or WP_Error if there was an issue getting the image.
	 */
	public static function set_post_thumbnail( $post_id, $image_url, $file_name = null, $desc = null ) {
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );

		$tmp = download_url( $image_url );

		if ( is_wp_error( $tmp ) ) {
			@unlink( $tmp );
			return $tmp;
		}

		preg_match( '/[^\?]+\.(jpg)/', $image_url, $matches );
		$file_array = array(
			'name' => basename( $matches[0] ),
			'tmp_name' => $tmp,
		);

		// If prepend nice filename if it is set
		if ( isset( $file_name ) ) {
			$file_array['name'] = $file_name . '-' . $file_array['name'];
		}

		$thumb_id = media_handle_sideload( $file_array, $post_id, $desc );

		if ( is_wp_error( $thumb_id ) ) {
			@unlink( $file_array['tmp_name'] );
			return $thumb_id;
		}

		return set_post_thumbnail( $post_id, $thumb_id );
	}

	/**
	 * Generates and returns an embed config ID used to display a publication.
	 *
	 * @since 1.0.0
	 *
	 * @param string $document_id Unique document ID. Required
	 * @param int $width          Expected embed width. Required
	 * @param int $height         Expected embed height. Required
	 * @param int $start_page     Document start page index. Optional. Default 1
	 *
	 * @return string
	 */
	public static function get_embed_config_id( $document_id, $width, $height, $start_page = 0 ) {

		if ( $start_page < 1 ) {
			$start_page = 1;
		}

		$api_key        = Issuu_Publication_Feed_Settings::get_api_key();
		$api_secret_key = Issuu_Publication_Feed_Settings::get_api_secret_key();
		$api_url        = Issuu_Publication_Feed_Settings::get_api_url();

		include_once 'lib/issuu-api.php';
		$issuu = new IssuuClient( $api_key, $api_secret_key, $api_url );

		$issuu->openAction( 'issuu.document_embed.add' );
		$issuu->__set( 'documentId', $document_id );
		$issuu->__set( 'width', $width );
		$issuu->__set( 'height', $height );
		$issuu->__set( 'readerStartPage', $start_page );
		$issuu->executeAction();
		$response = $issuu->getResponse();
		$issuu->closeAction();

		if ( 'ok' == $response['rsp']['stat'] ) {
			return $response['rsp']['_content']['documentEmbed']['dataConfigId'];
		}
		return false;
	}

}
